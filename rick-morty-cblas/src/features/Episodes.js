import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import ImageIcon from '@material-ui/icons/Image';
import WorkIcon from '@material-ui/icons/Work';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import MovieIcon from '@material-ui/icons/Movie';
import PeopleAltIcon from '@material-ui/icons/PeopleAlt';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper,
  },
}));

export default function Epidodes() {
  const classes = useStyles();

  return (
    <List className={classes.root}>
      <ListItem>
        <ListItemAvatar>
          <Avatar>
            <MovieIcon />
          </Avatar>
        </ListItemAvatar>
        <ListItemText primary="S01E01 - Pilots" secondary="December 2, 2013" />
        <ListItemSecondaryAction>
            <Tooltip title="Chapters" placement="right">
                    <IconButton edge="end" aria-label="delete">
                      <PeopleAltIcon />
                    </IconButton>
                    </Tooltip>
                  </ListItemSecondaryAction>

      </ListItem>
      <ListItem>
        <ListItemAvatar>
          <Avatar>
            <MovieIcon />
          </Avatar>
        </ListItemAvatar>
        <ListItemText primary="Work" secondary="Jan 7, 2014" />
        <ListItemSecondaryAction>
            <Tooltip title="Characters  " placement="right">
                    <IconButton edge="end" aria-label="delete">
                      <PeopleAltIcon />
                    </IconButton>
                    </Tooltip>
                  </ListItemSecondaryAction>

      </ListItem>
      <ListItem>
        <ListItemAvatar>
          <Avatar>
            <MovieIcon />
          </Avatar>
        </ListItemAvatar>
        <ListItemText primary="Vacation" secondary="July 20, 2014" />
        <ListItemSecondaryAction>
            <Tooltip title="Characters  " placement="right">
                    <IconButton edge="end" aria-label="delete">
                      <PeopleAltIcon />
                    </IconButton>
                    </Tooltip>
                  </ListItemSecondaryAction>

      </ListItem>
    </List>
  );
}
